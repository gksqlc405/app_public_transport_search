### 서울특별시_대중교통환승경로 조회 서비스 APP
###### 공공데이터 오픈 API 사용
### CORS 로 인해 목업 데이터로 구현
![Transport](./images/public1.png)

***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```

* 서울특별시 대중교통 환승경로 조회
  - 출발지와 목적지를 설정해 버스 이용 경로 확인
  - 지하철 환승경로 확인
  
```


# app 화면

### 메인화면
![Transport](./images/public2.png)


### 출발지 또는 도착지 검색
![Transport](./images/public3.png)

### 검색한 곧 정류장 이름들
![Transport](./images/public4.png)

### 버스 지하철 경로 검색결과
![Transport](./images/public5.png)


