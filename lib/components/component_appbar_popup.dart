import 'package:flutter/material.dart';

class ComponentAppbarPopup extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarPopup({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      automaticallyImplyLeading: false,
      iconTheme: const IconThemeData(
        color: Colors.black,
      ),
      backgroundColor: Color.fromRGBO(252, 186, 3, 10),
      title: Text(
        title,
        style: const TextStyle(
          color: Color.fromRGBO(58, 29, 29 ,10),
          fontSize: 16,
          fontFamily: 'maple'
        ),
      ),
      elevation: 1,
      actions: [
        IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.clear)),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(40);
}
