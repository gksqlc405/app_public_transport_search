import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';

class ComponentCustomLoading extends StatefulWidget {
  const ComponentCustomLoading({super.key, required this.cancelFunc});

  final CancelFunc cancelFunc;

  @override
  State<ComponentCustomLoading> createState() => _ComponentCustomLoadingState();
}

class _ComponentCustomLoadingState extends State<ComponentCustomLoading> with SingleTickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
      duration: const Duration(
        milliseconds: 1000,
      ),
      vsync: this,
    );

    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animationController.forward();
      }
    });
    animationController.forward();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.deepOrange,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 16,
          bottom: 16,
          left: 28,
          right: 28,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FadeTransition(
              opacity: animationController,
              child: Image.asset(
                'assets/logo.png',
                width: 25,
              ),
            ),
            SizedBox(height: 10,),
            Text('Loading...'),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
