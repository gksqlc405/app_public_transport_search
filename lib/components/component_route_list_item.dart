import 'package:app_public_transport_search/model/path_info_route_list.dart';

import 'package:flutter/material.dart';

class ComponentRouteListItem extends StatelessWidget {
  const ComponentRouteListItem({super.key, required this.item});

  final PathInfoRouteList item;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          for (int i = 0; i < item.pathList.length; i++)
            Text('${item.pathList[i].isBus ? '[버스]' : '[지하철]'} ${item.pathList[i].routeNm} ${item.pathList[i].fname} -> ${item.pathList[i].tname}',
            style: TextStyle(
              fontFamily: 'maple'
            ),
            ),
          Text('총 소요시간 : ${item.time}분 / 거리 : ${item.distance}km',
            style: TextStyle(
              fontFamily: 'kakaoBold'
            ),
          ),
        ],
      ),
    );
  }
}
