class LocationInfoListItem {
  int poiId;
  String poiNm;
  num gpsX;
  num gpsY;

  LocationInfoListItem(this.poiId, this.poiNm, this.gpsX, this.gpsY);

  factory LocationInfoListItem.fromJson(Map<String, dynamic> json) {
    return LocationInfoListItem(
      int.parse(json['poiId']),
      json['poiNm'],
      num.parse(json['gpsX']),
      num.parse(json['gpsY']),
    );
  }
}
