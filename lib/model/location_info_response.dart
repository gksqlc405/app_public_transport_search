import 'package:app_public_transport_search/model/location_info_list.dart';


class LocationInfoResponse {
  LocationInfoList msgBody;

  LocationInfoResponse(this.msgBody);

  factory LocationInfoResponse.fromJson(Map<String, dynamic> json) {
    return LocationInfoResponse(
      LocationInfoList.fromJson(json['msgBody']),
    );
  }
}