class PathInfoRequest {
  num startX;
  num startY;
  num endX;
  num endY;

  PathInfoRequest(this.startX, this.startY, this.endX, this.endY);
}