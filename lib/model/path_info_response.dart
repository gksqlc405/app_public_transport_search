import 'package:app_public_transport_search/model/path_info_route_response.dart';


class PathInfoResponse {
  PathInfoRouteResponse msgBody;

  PathInfoResponse(this.msgBody);

  factory PathInfoResponse.fromJson(Map<String, dynamic> json) {
    return PathInfoResponse(
      PathInfoRouteResponse.fromJson(json['msgBody']),
    );
  }
}