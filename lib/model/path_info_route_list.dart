import 'package:app_public_transport_search/model/path_info_route_list_item.dart';


class PathInfoRouteList {
  int time;
  num distance;
  List<PathInfoRouteListItem> pathList;


  PathInfoRouteList(this.time, this.distance, this.pathList);

  factory PathInfoRouteList.fromJson(Map<String, dynamic> json) {
    return PathInfoRouteList(
      int.parse(json['time']),
      (num.parse(json['distance']) / 1000),
      json['pathList'] == null ? [] : (json['pathList'] as List).map((e) => PathInfoRouteListItem.fromJson(e)).toList(),
    );
  }
}