import 'package:app_public_transport_search/model/path_info_route_list_item_rail_item.dart';


class PathInfoRouteListItem {
  String routeNm;
  String fname;
  String tname;
  bool isBus;

  PathInfoRouteListItem(this.routeNm, this.fname, this.tname, this.isBus);

  factory PathInfoRouteListItem.fromJson(Map<String, dynamic> json) {
    // todo : 3줄을 1줄로 줄여보기
    List<PathInfoRouteListItemRailItem> railItems = json['railLinkList'] == null ? [] : (json['railLinkList'] as List).map((e) => PathInfoRouteListItemRailItem.fromJson(e)).toList();
    bool isBus = true;
    if (railItems.length > 0) isBus = false;

    return PathInfoRouteListItem(
      json['routeNm'],
      json['fname'],
      json['tname'],
      isBus,
    );
  }
}