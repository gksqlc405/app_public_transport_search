class PathInfoRouteListItemRailItem {
  String railLinkId;

  PathInfoRouteListItemRailItem(this.railLinkId);

  factory PathInfoRouteListItemRailItem.fromJson(Map<String, dynamic> json) {
    return PathInfoRouteListItemRailItem(
      json['railLinkId'],
    );
  }
}