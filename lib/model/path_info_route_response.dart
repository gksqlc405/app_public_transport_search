import 'package:app_public_transport_search/model/path_info_route_list.dart';


class PathInfoRouteResponse {
  List<PathInfoRouteList> itemList;

  PathInfoRouteResponse(this.itemList);

  factory PathInfoRouteResponse.fromJson(Map<String, dynamic> json) {
    return PathInfoRouteResponse(
        json['itemList'] == null ? [] : (json['itemList'] as List).map((e) => PathInfoRouteList.fromJson(e)).toList(),
    );
  }
}