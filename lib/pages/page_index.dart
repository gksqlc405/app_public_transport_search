import 'package:app_public_transport_search/components/component_appbar_filter.dart';
import 'package:app_public_transport_search/components/component_count_title.dart';
import 'package:app_public_transport_search/components/component_no_contents.dart';
import 'package:app_public_transport_search/components/component_route_list_item.dart';
import 'package:app_public_transport_search/model/path_info_request.dart';
import 'package:app_public_transport_search/model/path_info_route_list.dart';
import 'package:app_public_transport_search/pages/page_search_location.dart';
import 'package:app_public_transport_search/repository/repo_path_info.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();

  String startAreaName = '';
  num startAreaX = 0;
  num startAreaY = 0;
  String endAreaName = '';
  num endAreaX = 0;
  num endAreaY = 0;

  List<PathInfoRouteList> _itemList = [];

  Future<void> _loadItems() async {
    if (startAreaX != 0 && startAreaY != 0 && endAreaX != 0 && endAreaY != 0) {
      PathInfoRequest request =
      PathInfoRequest(startAreaX, startAreaY, endAreaX, endAreaY);

      await RepoPathInfo().getList(request).then((res) {
        setState(() {
          _itemList = res.msgBody.itemList;
        });
      }).catchError((err) => debugPrint(err));

      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    } else {
      BotToast.showSimpleNotification(
          title: "출발지와 도착지는 필수입니다.",
          subTitle: "출발지와 도착지를 먼저 전부 검색해주세요.",
          enableSlideOff: true,
          hideCloseButton: false,
          onlyOne: true,
          crossPage: true,
          backButtonBehavior: BackButtonBehavior.none,
          animationDuration: Duration(milliseconds: 200),
          animationReverseDuration:
              Duration(milliseconds: 200),
          duration: Duration(seconds: 2));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          Image.asset('kakao.png',
          ),
          SizedBox(height: 30,),
          _buildForm(),
          const Divider(color: Colors.black,),
          _buildList(),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return Column(
      children: [
        const Text('출발지',
        style: TextStyle(
          fontFamily: 'maple',
          fontSize: 20,
        ),
        ),
        Row(
          children: [
            Container(
              child: Text(startAreaName),
            ),
            IconButton(
                onPressed: () async {
                  final popupResult = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageSearchLocation(isStart: true,)
                    )
                  );

                  if (popupResult != null && popupResult[0] && popupResult[1]) {
                    setState(() {
                      startAreaName = popupResult[2];
                      startAreaX = popupResult[3];
                      startAreaY = popupResult[4];
                    });
                  }
                },
                icon: const Icon(Icons.search),
            ),
          ],
        ),
        const SizedBox(height: 15,),
        const Text('도착지',
          style: TextStyle(
            fontFamily: 'maple',
            fontSize: 20,
          ),
        ),
        Row(
          children: [
            Container(
              child: Text(endAreaName),
            ),
            IconButton(
              onPressed: () async {
                final popupResult = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PageSearchLocation(isStart: false,)
                    )
                );

                if (popupResult != null && popupResult[0] && !popupResult[1]) {
                  setState(() {
                    endAreaName = popupResult[2];
                    endAreaX = popupResult[3];
                    endAreaY = popupResult[4];
                  });
                }
              },
              icon: const Icon(Icons.search),
            ),
          ],
        ),
        const SizedBox(height: 15,),
        ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Color.fromRGBO(247, 231, 0, 10)),
            ),
            onPressed: () {
              _loadItems();
            },
            child: const Text('조회',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 15,
                color: Color.fromRGBO(58, 29, 29, 10)
              ),
            )
        ),
      ],
    );
  }

  Widget _buildList() {
    if (_itemList.length > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(
            icon: Icons.abc,
            count: _itemList.length,
            unitName: '건',
            itemName: '경로',
          ),
          const SizedBox(height: 10,),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 5,
            itemBuilder: (_, index) => ComponentRouteListItem(item: _itemList[index]),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 40,
        child: const ComponentNoContents(icon: Icons.access_alarms_outlined, msg: '경로가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}
