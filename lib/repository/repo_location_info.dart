import 'package:app_public_transport_search/model/location_info_response.dart';


class RepoLocationInfo {
  Future<LocationInfoResponse> getList(String searchKeyword) async {
    // 서대문
    var resultKeyword1 = {
      "comMsgHeader": {
        "errMsg": null,
        "responseMsgID": null,
        "responseTime": null,
        "requestMsgID": null,
        "returnCode": null,
        "successYN": null
      },
      "msgHeader": {
        "headerMsg": "정상적으로 처리되었습니다.",
        "headerCd": "0",
        "itemCount": 0
      },
      "msgBody": {
        "itemList": [
          {
            "poiId": "112000314",
            "poiNm": "독립문역.서대문형무소역사관",
            "gpsX": "126.955308749",
            "gpsY": "37.5763938404",
            "posX": "196052.69273101116",
            "posY": "452988.8812064999",
            "addr": null
          },
          {
            "poiId": "100000072",
            "poiNm": "독립문역.서대문형무소역사관",
            "gpsX": "126.9594644024",
            "gpsY": "37.5732126863",
            "posX": "196419.5841111665",
            "posY": "452635.68115442107",
            "addr": null
          },
          {
            "poiId": "100000073",
            "poiNm": "서대문.서울시교육청",
            "gpsX": "126.9645273358",
            "gpsY": "37.5682538056",
            "posX": "196866.57361313395",
            "posY": "452085.1810826338",
            "addr": null
          },
          {
            "poiId": "100900044",
            "poiNm": "서대문3번출구",
            "gpsX": "126.9662183105",
            "gpsY": "37.5664160731",
            "posX": "197015.8701024566",
            "posY": "451881.1810500114",
            "addr": null
          },
          {
            "poiId": "112000169",
            "poiNm": "서대문경찰서",
            "gpsX": "126.968395",
            "gpsY": "37.563413",
            "posX": "197208.03746444537",
            "posY": "451547.84396421164",
            "addr": null
          },
          {
            "poiId": "101000157",
            "poiNm": "서대문경찰서.농협은행.유관순활동터",
            "gpsX": "126.9675115679",
            "gpsY": "37.5652819605",
            "posX": "197130.06741953862",
            "posY": "451755.2810342419",
            "addr": null
          },
          {
            "poiId": "101000020",
            "poiNm": "서대문경찰서.농협은행.유관순활동터",
            "gpsX": "126.9677868059",
            "gpsY": "37.5649062803",
            "posX": "197154.36684510036",
            "posY": "451713.58103308873",
            "addr": null
          },
          {
            "poiId": "112000076",
            "poiNm": "서대문구보건소별관",
            "gpsX": "126.9363679674",
            "gpsY": "37.5825648287",
            "posX": "194380.23201800906",
            "posY": "453674.68132211594",
            "addr": null
          },
          {
            "poiId": "112000142",
            "poiNm": "서대문구보훈회관",
            "gpsX": "126.9295369529",
            "gpsY": "37.5718307606",
            "posX": "193776.0462094741",
            "posY": "452483.88116895454",
            "addr": null
          },
          {
            "poiId": "112000141",
            "poiNm": "서대문구보훈회관",
            "gpsX": "126.9293638634",
            "gpsY": "37.5717018011",
            "posX": "193760.7465692428",
            "posY": "452469.58117006766",
            "addr": null
          },
          {
            "poiId": "112000125",
            "poiNm": "서대문구청",
            "gpsX": "126.934871913",
            "gpsY": "37.5790506621",
            "posX": "194247.83512316752",
            "posY": "453284.78126844764",
            "addr": null
          },
          {
            "poiId": "112000073",
            "poiNm": "서대문구청.보건소.구의회",
            "gpsX": "126.9360659508",
            "gpsY": "37.5782448388",
            "posX": "194353.23265334603",
            "posY": "453195.2812574948",
            "addr": null
          },
          {
            "poiId": "112000074",
            "poiNm": "서대문구청.서대문구의회",
            "gpsX": "126.9358425895",
            "gpsY": "37.5786159669",
            "posX": "194333.53311560312",
            "posY": "453236.48125956114",
            "addr": null
          },
          {
            "poiId": "112900017",
            "poiNm": "서대문등기소",
            "gpsX": "126.9334962806",
            "gpsY": "37.5803213337",
            "posX": "194126.43797539806",
            "posY": "453425.8812937732",
            "addr": null
          },
          {
            "poiId": "112900088",
            "poiNm": "서대문등기소",
            "gpsX": "126.9333967449",
            "gpsY": "37.5802086413",
            "posX": "194117.6381845038",
            "posY": "453413.381292121",
            "addr": null
          },
          {
            "poiId": "112900037",
            "poiNm": "서대문로터리",
            "gpsX": "126.9669874701",
            "gpsY": "37.5652394638",
            "posX": "197083.76850264624",
            "posY": "451750.58103782544",
            "addr": null
          },
          {
            "poiId": "112900013",
            "poiNm": "서대문문화체육회관",
            "gpsX": "126.9322126182",
            "gpsY": "37.5812559352",
            "posX": "194013.14063638676",
            "posY": "453529.6813114146",
            "addr": null
          },
          {
            "poiId": "112000256",
            "poiNm": "서대문문화체육회관",
            "gpsX": "126.9349134591",
            "gpsY": "37.5807483392",
            "posX": "194251.63503531125",
            "posY": "453473.18129368965",
            "addr": null
          },
          {
            "poiId": "112900098",
            "poiNm": "서대문문화체육회관",
            "gpsX": "126.9319952523",
            "gpsY": "37.5812287775",
            "posX": "193993.94108788556",
            "posY": "453526.6813136479",
            "addr": null
          },
          {
            "poiId": "112000118",
            "poiNm": "서대문문화체육회관입구",
            "gpsX": "126.9353906921",
            "gpsY": "37.5814081994",
            "posX": "194293.834047274",
            "posY": "453546.3813085011",
            "addr": null
          },
          {
            "poiId": "112900163",
            "poiNm": "서대문사거리",
            "gpsX": "126.9654574653",
            "gpsY": "37.5666951917",
            "posX": "196948.6716768905",
            "posY": "451912.1810637461",
            "addr": null
          },
          {
            "poiId": "112900173",
            "poiNm": "서대문성당",
            "gpsX": "126.9640444243",
            "gpsY": "37.5672714689",
            "posX": "196823.87461228657",
            "posY": "451976.18106556777",
            "addr": null
          },
          {
            "poiId": "112900175",
            "poiNm": "서대문센트레빌",
            "gpsX": "126.9639357971",
            "gpsY": "37.5671723156",
            "posX": "196814.27483674005",
            "posY": "451965.1810633051",
            "addr": null
          },
          {
            "poiId": "112000403",
            "poiNm": "서대문역",
            "gpsX": "126.9658300015",
            "gpsY": "37.5664745324",
            "posX": "196981.57090488283",
            "posY": "451887.68105468014",
            "addr": null
          },
          {
            "poiId": "101000304",
            "poiNm": "서대문역사거리",
            "gpsX": "126.966565",
            "gpsY": "37.565579",
            "posX": "197046.4621196362",
            "posY": "451788.2747343248",
            "addr": null
          },
          {
            "poiId": "101000305",
            "poiNm": "서대문역사거리",
            "gpsX": "126.966893",
            "gpsY": "37.566137",
            "posX": "197075.4583768209",
            "posY": "451850.18930908013",
            "addr": null
          },
          {
            "poiId": "101000262",
            "poiNm": "서대문역사거리.농협중앙회",
            "gpsX": "126.9678855112",
            "gpsY": "37.5669222408",
            "posX": "197163.1626786932",
            "posY": "451937.30209408747",
            "addr": null
          },
          {
            "poiId": "100000368",
            "poiNm": "서대문역사거리.적십자병원",
            "gpsX": "126.9675878497",
            "gpsY": "37.5671081521",
            "posX": "197136.87582877823",
            "posY": "451957.94289245876",
            "addr": null
          },
          {
            "poiId": "112000011",
            "poiNm": "서대문우체국",
            "gpsX": "126.9314621185",
            "gpsY": "37.5625326219",
            "posX": "193945.3422372501",
            "posY": "451451.88103455026",
            "addr": null
          },
          {
            "poiId": "112000010",
            "poiNm": "서대문우체국",
            "gpsX": "126.9320446385",
            "gpsY": "37.5617697328",
            "posX": "193996.74102579433",
            "posY": "451367.18102080515",
            "addr": null
          },
          {
            "poiId": "112000030",
            "poiNm": "서대문우체국",
            "gpsX": "126.9311628502",
            "gpsY": "37.5630010161",
            "posX": "193918.94285235903",
            "posY": "451503.8810394532",
            "addr": null
          },
          {
            "poiId": "112000029",
            "poiNm": "서대문우체국",
            "gpsX": "126.9303828201",
            "gpsY": "37.5631140964",
            "posX": "193850.04446929565",
            "posY": "451516.4810365988",
            "addr": null
          },
          {
            "poiId": "112000071",
            "poiNm": "서대문자연사박물관입구",
            "gpsX": "126.9355048737",
            "gpsY": "37.5750483644",
            "posX": "194303.43382302945",
            "posY": "452840.5812079804",
            "addr": null
          },
          {
            "poiId": "112000072",
            "poiNm": "서대문자연사박물관입구",
            "gpsX": "126.9351390733",
            "gpsY": "37.5751824266",
            "posX": "194271.13457606206",
            "posY": "452855.4812146663",
            "addr": null
          },
          {
            "poiId": "112900054",
            "poiNm": "서대문전철역",
            "gpsX": "126.9659992155",
            "gpsY": "37.5652797325",
            "posX": "196996.4705611856",
            "posY": "451755.08103798144",
            "addr": null
          },
          {
            "poiId": "112000178",
            "poiNm": "서대문종합사회복지관",
            "gpsX": "126.9201437133",
            "gpsY": "37.5709633371",
            "posX": "192946.26570076233",
            "posY": "452388.28116858564",
            "addr": null
          },
          {
            "poiId": "112000177",
            "poiNm": "서대문종합사회복지관",
            "gpsX": "126.9200245273",
            "gpsY": "37.5712597156",
            "posX": "192935.76595141145",
            "posY": "452421.1811778932",
            "addr": null
          },
          {
            "poiId": "112900190",
            "poiNm": "서대문지역자활센터",
            "gpsX": "126.9262762163",
            "gpsY": "37.5721234264",
            "posX": "193488.05297093312",
            "posY": "452516.5811840701",
            "addr": null
          },
          {
            "poiId": "112000116",
            "poiNm": "신연중학교서대문도서관",
            "gpsX": "126.9408336411",
            "gpsY": "37.583957533",
            "posX": "194774.72274550385",
            "posY": "453828.9813355361",
            "addr": null
          },
          {
            "poiId": "112000070",
            "poiNm": "한성화교중고교.서대문소방서",
            "gpsX": "126.935209346",
            "gpsY": "37.5724773892",
            "posX": "194277.13443492484",
            "posY": "452555.28117149835",
            "addr": null
          },
          {
            "poiId": "112000069",
            "poiNm": "한성화교중고교.서대문소방서",
            "gpsX": "126.9354132782",
            "gpsY": "37.572300887",
            "posX": "194295.13401176568",
            "posY": "452535.68116734596",
            "addr": null
          },
          {
            "poiId": "112000144",
            "poiNm": "홍남교.서대문두바퀴환경센터",
            "gpsX": "126.9271612879",
            "gpsY": "37.5746884815",
            "posX": "193566.45113101034",
            "posY": "452801.18121269206",
            "addr": null
          },
          {
            "poiId": "112000143",
            "poiNm": "홍남교.서대문두바퀴환경센터",
            "gpsX": "126.9267785046",
            "gpsY": "37.574809892",
            "posX": "193532.65192559856",
            "posY": "452814.6812144965",
            "addr": null
          },
          {
            "poiId": "112000407",
            "poiNm": "홍제역.서대문세무서",
            "gpsX": "126.944365257",
            "gpsY": "37.5884908289",
            "posX": "195086.91541855794",
            "posY": "454331.8813999556",
            "addr": null
          },
          {
            "poiId": "112000408",
            "poiNm": "홍제역.서대문세무서",
            "gpsX": "126.9453225684",
            "gpsY": "37.5878433932",
            "posX": "195171.4134327716",
            "posY": "454259.9813961196",
            "addr": null
          }
        ]
      }
    };
    // 서울대
    var resultKeyword2 = {
      "comMsgHeader": {
        "requestMsgID": null,
        "responseMsgID": null,
        "responseTime": null,
        "returnCode": null,
        "successYN": null,
        "errMsg": null
      },
      "msgHeader": {
        "headerMsg": "정상적으로 처리되었습니다.",
        "headerCd": "0",
        "itemCount": 0
      },
      "msgBody": {
        "itemList": [
          {
            "poiId": "120000086",
            "poiNm": "e편한세상서울대입구아파트",
            "gpsX": "126.9429191694",
            "gpsY": "37.4797702703",
            "posX": "194951.88872018794",
            "posY": "442266.6053214222",
            "addr": null
          },
          {
            "poiId": "120000080",
            "poiNm": "e편한세상서울대입구아파트",
            "gpsX": "126.9432329537",
            "gpsY": "37.4794530021",
            "posX": "194979.61793426165",
            "posY": "442231.3797728638",
            "addr": null
          },
          {
            "poiId": "285500040",
            "poiNm": "남서울대학",
            "gpsX": "127.1422387045",
            "gpsY": "36.9101782311",
            "posX": "212674.16256654478",
            "posY": "379067.21131595597",
            "addr": null
          },
          {
            "poiId": "277104605",
            "poiNm": "동서울대(경유)",
            "gpsX": "127.1256921832",
            "gpsY": "37.4598591226",
            "posX": "211118.9089991493",
            "posY": "440062.86140077235",
            "addr": null
          },
          {
            "poiId": "277104604",
            "poiNm": "동서울대(경유)",
            "gpsX": "127.1261541114",
            "gpsY": "37.4594580905",
            "posX": "211159.83143997448",
            "posY": "440018.4116958142",
            "addr": null
          },
          {
            "poiId": "204000112",
            "poiNm": "동서울대학교",
            "gpsX": "127.1256549347",
            "gpsY": "37.4604054077",
            "posX": "211115.53307725055",
            "posY": "440123.48071112717",
            "addr": null
          },
          {
            "poiId": "204000116",
            "poiNm": "동서울대학교",
            "gpsX": "127.1261383341",
            "gpsY": "37.4589969382",
            "posX": "211158.50427405548",
            "posY": "439967.2336967597",
            "addr": null
          },
          {
            "poiId": "113000061",
            "poiNm": "서울대동창회관",
            "gpsX": "126.9507441044",
            "gpsY": "37.542444957",
            "posX": "195647.5509268119",
            "posY": "449221.56173717696",
            "addr": null
          },
          {
            "poiId": "100900076",
            "poiNm": "서울대병원장례식장",
            "gpsX": "126.9974309648",
            "gpsY": "37.5805553988",
            "posX": "199773.1053298805",
            "posY": "453449.78125799587",
            "addr": null
          },
          {
            "poiId": "224001009",
            "poiNm": "서울대시흥캠퍼스",
            "gpsX": "126.7206703518",
            "gpsY": "37.3650010581",
            "posX": "175258.90170618857",
            "posY": "429565.31190578453",
            "addr": null
          },
          {
            "poiId": "224001008",
            "poiNm": "서울대시흥캠퍼스",
            "gpsX": "126.7208140322",
            "gpsY": "37.3656024022",
            "posX": "175271.82530425745",
            "posY": "429632.0072105462",
            "addr": null
          },
          {
            "poiId": "224001000",
            "poiNm": "서울대시흥캠퍼스.한라1차",
            "gpsX": "126.7133168116",
            "gpsY": "37.3664449982",
            "posX": "174608.06088917347",
            "posY": "429727.5027668439",
            "addr": null
          },
          {
            "poiId": "100900085",
            "poiNm": "서울대어린이병원응급실.갑상선센터",
            "gpsX": "126.9995029662",
            "gpsY": "37.5781188768",
            "posX": "199956.10103153923",
            "posY": "453179.38122060196",
            "addr": null
          },
          {
            "poiId": "120000228",
            "poiNm": "서울대입구역",
            "gpsX": "126.9524440164",
            "gpsY": "37.4807005941",
            "posX": "195794.29880191613",
            "posY": "442369.37978620967",
            "addr": null
          },
          {
            "poiId": "120000150",
            "poiNm": "서울대입구역",
            "gpsX": "126.9526785586",
            "gpsY": "37.4799356496",
            "posX": "195814.99831256855",
            "posY": "442284.47977247695",
            "addr": null
          },
          {
            "poiId": "120900050",
            "poiNm": "서울대입구역2호선",
            "gpsX": "126.9538359816",
            "gpsY": "37.4824664116",
            "posX": "195917.49589940277",
            "posY": "442565.27980993036",
            "addr": null
          },
          {
            "poiId": "120000005",
            "poiNm": "서울대입구역5번출구",
            "gpsX": "126.951014176",
            "gpsY": "37.4816281486",
            "posX": "195667.90177146063",
            "posY": "442472.37980224844",
            "addr": null
          },
          {
            "poiId": "120000197",
            "poiNm": "서울대정문",
            "gpsX": "126.9487039701",
            "gpsY": "37.4652441487",
            "posX": "195462.60658632717",
            "posY": "440654.2795590521",
            "addr": null
          },
          {
            "poiId": "120000167",
            "poiNm": "서울대정문",
            "gpsX": "126.9482401845",
            "gpsY": "37.4656566538",
            "posX": "195421.60755600058",
            "posY": "440700.0795582053",
            "addr": null
          },
          {
            "poiId": "100900030",
            "poiNm": "서울대치과대학",
            "gpsX": "126.9978748922",
            "gpsY": "37.5773060748",
            "posX": "199812.3044058556",
            "posY": "453089.18120827153",
            "addr": null
          },
          {
            "poiId": "100900038",
            "poiNm": "서울대치과대학",
            "gpsX": "126.9977741191",
            "gpsY": "37.5776016307",
            "posX": "199803.4046147555",
            "posY": "453121.9812078243",
            "addr": null
          },
          {
            "poiId": "120000028",
            "poiNm": "서울대학교",
            "gpsX": "126.9479522861",
            "gpsY": "37.4667414611",
            "posX": "195396.2081475957",
            "posY": "440820.4795799181",
            "addr": null
          },
          {
            "poiId": "120000029",
            "poiNm": "서울대학교",
            "gpsX": "126.948727",
            "gpsY": "37.466827",
            "posX": "195464.73931798566",
            "posY": "440829.9346362436",
            "addr": null
          },
          {
            "poiId": "120000400",
            "poiNm": "서울대학교",
            "gpsX": "126.9489515813",
            "gpsY": "37.4668572388",
            "posX": "195484.60607171716",
            "posY": "440833.2795834802",
            "addr": null
          },
          {
            "poiId": "120000670",
            "poiNm": "서울대학교",
            "gpsX": "126.9496319032",
            "gpsY": "37.4676609636",
            "posX": "195544.83039503003",
            "posY": "440922.4401903092",
            "addr": null
          },
          {
            "poiId": "120000668",
            "poiNm": "서울대학교.치과병원.동물병원",
            "gpsX": "126.951937",
            "gpsY": "37.470034",
            "posX": "195748.85570931862",
            "posY": "441185.68038405804",
            "addr": null
          },
          {
            "poiId": "120000665",
            "poiNm": "서울대학교.치과병원.동물병원",
            "gpsX": "126.9520815656",
            "gpsY": "37.4692744185",
            "posX": "195761.5995651643",
            "posY": "441101.37961468566",
            "addr": null
          },
          {
            "poiId": "206000723",
            "poiNm": "서울대학교병원",
            "gpsX": "127.1219759363",
            "gpsY": "37.3507052587",
            "posX": "210805.8293515399",
            "posY": "427949.2484543831",
            "addr": null
          },
          {
            "poiId": "206000724",
            "poiNm": "서울대학교병원",
            "gpsX": "127.1219829864",
            "gpsY": "37.3504804902",
            "posX": "210806.48613748155",
            "posY": "427924.3061557519",
            "addr": null
          },
          {
            "poiId": "100900042",
            "poiNm": "서울대학교병원현관.암병원현관",
            "gpsX": "126.9981567653",
            "gpsY": "37.5789514712",
            "posX": "199837.2038261073",
            "posY": "453271.7812269158",
            "addr": null
          },
          {
            "poiId": "120000396",
            "poiNm": "서울대학교입구전철역",
            "gpsX": "126.9514280486",
            "gpsY": "37.4815931763",
            "posX": "195704.50090666508",
            "posY": "442468.4797959877",
            "addr": null
          },
          {
            "poiId": "120000431",
            "poiNm": "서울대호암교수회관",
            "gpsX": "126.9583174158",
            "gpsY": "37.4677295812",
            "posX": "196313.08661109357",
            "posY": "440929.67958813906",
            "addr": null
          },
          {
            "poiId": "120900159",
            "poiNm": "서울대후문.연구공원",
            "gpsX": "126.9576575658",
            "gpsY": "37.4650539577",
            "posX": "196254.58798691837",
            "posY": "440632.7795413798",
            "addr": null
          },
          {
            "poiId": "100000002",
            "poiNm": "창경궁.서울대학교병원",
            "gpsX": "126.9965660023",
            "gpsY": "37.5791830159",
            "posX": "199696.70712279636",
            "posY": "453297.481226997",
            "addr": null
          },
          {
            "poiId": "100000128",
            "poiNm": "창경궁.서울대학교병원",
            "gpsX": "126.9968717143",
            "gpsY": "37.5788279947",
            "posX": "199723.70649061544",
            "posY": "453258.08122246247",
            "addr": null
          },
          {
            "poiId": "206000722",
            "poiNm": "한국장애인고용공단.서울대병원 ",
            "gpsX": "127.1238883439",
            "gpsY": "37.3477689478",
            "posX": "210975.67676829948",
            "posY": "427623.61951843835",
            "addr": null
          },
          {
            "poiId": "206000721",
            "poiNm": "한국장애인고용공단.서울대병원 ",
            "gpsX": "127.1242489211",
            "gpsY": "37.347516123",
            "posX": "211007.65841508048",
            "posY": "427595.6049149879",
            "addr": null
          },
          {
            "poiId": "100000125",
            "poiNm": "혜화역.서울대병원입구",
            "gpsX": "127.0019395272",
            "gpsY": "37.5810636257",
            "posX": "200171.29597630168",
            "posY": "453506.18125720276",
            "addr": null
          }
        ]
      }
    };

    var resultJson = resultKeyword1;
    if (searchKeyword == '서울대') resultJson = resultKeyword2;

    return LocationInfoResponse.fromJson(resultJson);
  }
}